<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('sys.access.login');
})->name('sys.access.login');


Route::post('/access', ['\App\Http\Controllers\AccessController', 'authenticate'])->name('auth.access');
Route::post('/logout', ['\App\Http\Controllers\AccessController', 'logout'])->name('auth.logout');
Route::prefix('admin')->middleware(['auth', 'web', 'revalidate'])->group(function () {
    Route::get('/users', ['\App\Http\Controllers\UserController', 'index'])->name('user.index');
    Route::get('/users/novo', ['\App\Http\Controllers\UserController', 'create'])->name('user.create');
    Route::post('/users/novo', ['\App\Http\Controllers\UserController', 'store'])->name('user.store');
    Route::get('/users/{id}/edit', ['\App\Http\Controllers\UserController', 'edit'])->name('user.edit');
    Route::put('/users/{id}/edit', ['\App\Http\Controllers\UserController', 'update'])->name('user.update');
    Route::get('/users/{id}/delete', ['\App\Http\Controllers\UserController', 'destroy'])->name('user.delete');
});

