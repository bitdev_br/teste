@extends('layouts.masterpage')

@section('css')
<link rel="stylesheet" href="{{ asset('system/login/css/signin.css') }}" />
@endsection

@section('content')
<div class="container">

    <form name="frm_login" method="POST" action="{{ route('auth.access') }}" class="form-signin">
        @csrf
        <h2 class="form-signin-heading">Acessar Sistema</h2>
        <div class="form-group  @error('email') has-error @enderror">
            <label for="email" class="sr-only">Usuário</label>
            <input type="email" id="email" name="email" class="form-control" placeholder="Usuário" maxlength="75" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <div class="alert alert-danger">{{ $errors->first('email') }}</div>
            @endif
        </div>
        <div class="form-group  @error('password') has-error @enderror">
            <label for="password" class="sr-only">Senha</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Senha" maxlength="20" required>
            @if ($errors->has('password'))
                <div class="alert alert-danger">{{ $errors->first('password') }}</div>
            @endif
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Acessar</button>
    </form>

</div>
@endsection
