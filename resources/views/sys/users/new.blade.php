@extends('layouts.masterpage')

@section('content')
    <div class="container">
        <a href="{{ route('user.index') }}" class="btn btn-warning">Voltar</a>
        <hr />
        {!! Form::open(['route' => 'user.store', 'method' => 'POST']) !!}
        @csrf
        @include('sys.users._form')
        <button type="submit" class="btn btn-default">Salvar</button>
        {!! Form::close() !!}
    </div>
@endsection

