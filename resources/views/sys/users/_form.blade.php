<div class="form-group @error('name') has-error @enderror">
    {{ Form::label('name', 'Nome') }}
    {!! Form::text('name', $user->name ?? '', ['class' => 'form-control']) !!}
    @if ($errors->has('name'))
        <div class="alert alert-danger">{{ $errors->first('name') }}</div>
    @endif
</div>
<div class="form-group @error('email') has-error @enderror">
    {{ Form::label('email', 'Email') }}
    {!! Form::email('email', $user->email ?? '',['class' => 'form-control']) !!}
    @if ($errors->has('email'))
        <div class="alert alert-danger">{{ $errors->first('email') }}</div>
    @endif
</div>
<div class="form-group @error('password') has-error @enderror">
    {{ Form::label('password', 'Senha') }}
    {!! Form::password('password', ['class' => 'form-control']) !!}
    @if ($errors->has('password'))
        <div class="alert alert-danger">{{ $errors->first('password') }}</div>
    @endif
</div>
