@extends('layouts.masterpage')

@section('content')
    <div class="container">
        <a href="{{ route('user.create') }}" class="btn btn-primary">Novo Usuário</a>
        <hr />
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Usuario</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse($usuarios as $usuario)
                    <tr>
                        <td>{{ $usuario->name }}</td>
                        <td><a href="{{ route('user.edit', $usuario->id) }}" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">Não existem usuários cadastrados</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>

@endsection
