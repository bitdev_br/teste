@extends('layouts.masterpage')

@section('content')
    <div class="container">
        <a href="{{ route('user.index') }}" class="btn btn-warning">Voltar</a>
        <hr />
        {!! Form::model($user, ['route' => ['user.update', $user->id]]) !!}
            @csrf
            @method('PUT')
            @include('sys.users._form')
            <button type="submit" class="btn btn-default">Editar</button>
            <a onclick="return confirm('Tem certeza?')" href="{{ route('user.delete', $user->id) }}" class="btn btn-danger">Apagar</a>
        {!! Form::close() !!}
    </div>
@endsection

