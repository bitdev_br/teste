<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $verb = $this->getMethod();
        $validate = [];
        if($verb == 'POST') {
            $validate = [
                'name' => 'required|max:100',
                'email' => 'required|unique:users|max:75|email',
                'password' => 'required|max:20',
            ];
        }else{
            $validate = [
                'name' => 'required|max:100',
                'email' => 'required|max:75|email',
            ];
        }
        return $validate;
    }
}
