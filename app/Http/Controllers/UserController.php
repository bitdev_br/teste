<?php

namespace App\Http\Controllers;

use App\Helpers\TesteHelper;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
	private $_user;

	public function __construct(User $user)
	{
		$this->_user = $user;
	}

    public function index()
    {
        $usuarios = $this->_user->all();
        return view('sys.users.index', compact('usuarios'));
    }


    public function create()
    {
        return view('sys.users.new');
    }

    public function store(UserRequest $request)
    {
        $user = $request->validated();
		$this->_user->create($user);

        TesteHelper::setFlashMessage('Usuário criado com sucesso', 'alert-success');

        return redirect()->route('user.index');
    }

    public function edit($id)
    {
        $user = $this->_user->findOrFail($id);
		return view('sys.users.edit', compact('user'));
    }

    public function update(UserRequest $request, $id)
    {
        $user = $this->_user->findOrFail($id);
        $user->password = bcrypt($request->input('password'));
        $user->update($request->validated());

        TesteHelper::setFlashMessage('Usuário editado com sucesso', 'alert-success');

        return redirect()->route('user.index');
    }

    public function destroy($id)
    {
        $user = $this->_user->findOrFail($id);
		$user->delete();

        TesteHelper::setFlashMessage('Usuário apagado com sucesso', 'alert-success');

        return redirect()->route('user.index');
    }
}
