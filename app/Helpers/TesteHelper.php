<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Session;

class TesteHelper
{

    public static function setFlashMessage( $message, $cls = '')
    {
        Session::flash('message', $message);
        Session::flash('alert-class', $cls);
    }

}
